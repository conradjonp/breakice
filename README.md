# Breakice

To launch mongoDB:

mongod --dbpath "C:\Users\GuLLy\Dropbox\Work Related\Side Project\Startup Weekend\Aego\data"
Mac:
./mongod --dbpath /Users/cdjpantua/Dropbox/Work\ Related/Side\ Project/Startup\ Weekend/Aego/data/

mongo

To add field in your collection:

db.your_collection.update({},{$set : {"new_field":1}},false,true)

To create your collection:

db.usercollection.insert({ "username" : "testuser1", "email" : "testuser1@testdomain.com" })

To insert stuff in your collection:

newstuff = [{ "username" : "testuser2", "email" : "testuser2@testdomain.com" }, { "username" : "testuser3", "email" : "testuser3@testdomain.com" }]
db.usercollection.insert(newstuff);

To return collection items:

db.usercollection.find()

Resource:

http://cwbuecheler.com/web/tutorials/2013/node-expressA-mongo/
http://stackoverflow.com/questions/7714216/add-new-field-to-a-collection-in-mongodb
https://docs.mongodb.org/manual/reference/method/db.collection.update/
https://github.com/translationexchange/blog/blob/master/getting-started-with-node-js-express-ejs-bootstrap-and-tml/my-app/views/error.ejs
https://www.npmjs.com/package/ejs
https://docs.mongodb.org/v3.0/reference/method/db.collection.find/
https://github.com/ctavan/express-validator